<?php
/**
 * * Template Name: Homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package diabetes
 */

get_header();
?>

<section class="pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="color-green helvetica-cenarrow"><?php the_field('overview_title');?></h1>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                        <div class="text-left">
                            <?php echo the_field('overview_content');?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <img src="<?php echo the_field('doctors_card');?>" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="article-bg pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center color-grey"><?php echo the_field('article_title');?></h1>
                <div class="row mt-20 mt-xs-10 mt-sm-10">
                    <?php $sec = 0.1; ?>
                    <?php while(have_rows('article_section')): the_row(); ?>
                        <?php $sec=$sec+0.1; ?>
                        <div class="col-lg-4 col-md-4 wow fadeIn" data-wow-delay="<?php echo $sec; ?>s">
                            <div class="text-center mb-20">
                                <a class="scroll" href="#<?php the_sub_field('id');?>"><img src="<?php echo the_sub_field('image');?>" alt="" /></a>
                            </div>
                            <div class="row article-title-styling">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                    <p class="color-white Helvetica fs-14"><a class="scroll" href="#<?php the_sub_field('id');?>"><?php echo the_sub_field('article_chapter_title');?></a></p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-3 text-left">
                                    <a class="scroll" href="#<?php the_sub_field('id');?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="chapterone"><img src="<?php echo the_field('chapters_bg');?>" alt="" /> 
    <div class="banner-caption-form-pilar">
        <div class="container">
            <div class="row">
                <div class="col-12 pt-30">
                    <div class="row align-self-center">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-center">
                            <h1 class="color-white"><img src="<?php echo the_field('chapter_one_icon');?>" alt="" /> &nbsp;<?php echo the_field('chapter_one_title');?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3"></div>
                    <div class="col-lg-7 col-md-7 text-justify">
                        <?php echo the_field('chapter_one_content');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
