<?php
/**
 * diabetes functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package diabetes
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'diabetes_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function diabetes_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on diabetes, use a find and replace
		 * to change 'diabetes' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'diabetes', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'diabetes' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'diabetes_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'diabetes_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function diabetes_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'diabetes_content_width', 640 );
}
add_action( 'after_setup_theme', 'diabetes_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function diabetes_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'diabetes' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'diabetes' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'diabetes_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function diabetes_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/diabetes-pillar-assets/css/bootstrap.min.css', array(), '4.6.0' );
	wp_enqueue_style( 'swiper', get_template_directory_uri() . '/diabetes-pillar-assets/css/swiper.min.css', array(), '4.2.0' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/diabetes-pillar-assets/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'padd', get_template_directory_uri() . '/diabetes-pillar-assets/css/padd-mar.css', array(), '1.0' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/diabetes-pillar-assets/css/animate.css', array(), '3.7.0' );
	wp_enqueue_style( 'practo-style', get_stylesheet_uri(), '1.1' );
	
	wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/diabetes-pillar-assets/js/jquery.min.js', array(), '3.2.1' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/diabetes-pillar-assets/js/bootstrap.min.js', array(), '4.6.0', true );
	wp_enqueue_script( 'swiper', get_template_directory_uri() . '/diabetes-pillar-assets/js/swiper.min.js', array(), '4.2.0', true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/diabetes-pillar-assets/js/wow.min.js', array(), '1.1.3', true );
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/diabetes-pillar-assets/js/custom.js', array(), '1.0', true );
	$urlarrays = array( 'site_url' => get_site_url(),'template_url' => get_template_directory_uri() );
	wp_localize_script( 'custom','siteurls',$urlarrays );
	// wp_enqueue_script( 'diabetes-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'diabetes_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}