$(document).ready(function ($) {
    $.ajax({
        type: 'POST',
        url: 'https://lab.practo.com/ip.php',
        success: function (resultnew) {
            $('.ipcity').html(resultnew);
            // console.log(resultnew);
        }
    });
    selectbox();
    function selectbox() {
        $("select").change(function () {
            $(this).find("option:selected").each(function () {
                var optionValue = $(this).attr("value");
                if (optionValue) {
                    $(".box").not("." + optionValue).hide();
                    $("." + optionValue).show();
                } else {
                    $(".box").hide();
                }
            });
        }).change();
    }
    setTimeout(function () {
        $("#cities").modal('show');
    }, 100);
    $(".close").click(function () {
        $("#cities").modal('hide');
    });
    // $('#cityModal').modal({ keyboard: false })
    // $('.modal-body a').click(function() {
    //     $("#cityModal").modal('hide');
    //     $(".modal-backdrop").hide();
    // });
    $('#cities a').click(function () {
        var ide = $(this).attr('title');
        $('#inputGroupSelect01').val(ide);
        selectbox();
    })
    $('#inputGroupSelect01').on('change', function (e) {
        var valueSelected = this.value;
        $('#input_4_9').val(valueSelected);
    });

    $('#booknow1 a').click(function () {
        var idebook = $(this).attr('title');
        $('#booknow1').val(idebook);
    })
    $('#booknow1').on('change', function (e) {
        var valueSelected = this.value;
        $('#input_4_12').val(valueSelected);
    });

    $('#booknow1').click(function () {
        $('#input_4_12').val('Blood Test');
    })
    $('#booknow2').click(function () {
        $('#input_4_12').val('Blood Test');
    })
    $('#booknow3').click(function () {
       $('#input_4_12').val('Blood Test');
    })
    $('#booknow4').click(function () {
       $('#input_4_12').val('Blood Test');
    })
    $('#booknow5').click(function () {
       $('#input_4_12').val('Blood Test');
    })
    $('#booknow6').click(function () {
       $('#input_4_12').val('Blood Test');
    })

    $('#booknowpackages1').click(function () {
        $('#input_4_12').val('Customise Health Checkup');
    })
    $('#booknowpackages2').click(function () {
        $('#input_4_12').val('Customise Health Checkup');
    })
    $('#booknowpackages3').click(function () {
        $('#input_4_12').val("Customise Health Checkup");
    })
    $('#booknowpackages4').click(function () {
        $('#input_4_12').val('Customise Health Checkup');
    })
    $('#booknowpackages5').click(function () {
        $('#input_4_12').val('Customise Health Checkup');
    })

    // var city=$('#inputGroupSelect01').val();
    // $('#input_4_10').val(city);

  
// $('#inputGroupSelect01').on('change', function (e) {
//     alert("dsfds");
//     var optionSelected = $("option:selected", this);
//     var valueSelected = this.value;
//    alert(valueSelected);
// });
    var bannerswiper = new Swiper("#bannerswiper", {
        fadeEffect: {
            crossFade: true,
        },
        effect: "fade",
        loop: true,
        virtualTranslate: true,
        slidesPerView: "auto",
        spaceBetween: 10,
        centeredSlides: true,
        speed: 1000,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".fa-chevron-circle-right",
            prevEl: ".fa-chevron-circle-left",
        },

        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    var bannerswipermob = new Swiper("#bannerswiper-mob", {
        fadeEffect: {
            crossFade: true,
        },
        effect: "fade",
        virtualTranslate: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        speed: 1000,
        loop: true,
        slidesPerView: "auto",
        spaceBetween: 10,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },

        navigation: {
            nextEl: ".fa-chevron-circle-right",
            prevEl: ".fa-chevron-circle-left",
        },

        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    var testsswiper = new Swiper("#trending-tests-boxes", {
        fadeEffect: {
            crossFade: true,
        },
        loop: true,
        slidesPerView: "auto",
        spaceBetween: 15,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    var packagesSwiper = new Swiper("#trending-packages-boxes", {
        // direction: "horizontal",
        fadeEffect: {
            crossFade: true,
        },
        loop: true,
        slidesPerView: "auto",
        spaceBetween: 15,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },

        navigation: {
            nextEl: '.swiper-button-next1',
            prevEl: '.swiper-button-prev1',
        },

        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    var packagesSwiper = new Swiper("#testimonial", {
        // direction: "horizontal",
        fadeEffect: {
            crossFade: true,
        },
        loop: true,
        slidesPerView: "auto",
        spaceBetween: 15,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },

        // navigation: {
        //     nextEl: '.swiper-button-next1',
        //     prevEl: '.swiper-button-prev1',
        // },

        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    new WOW().init();
});
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200); // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200); // Else fade out the arrow
    }
});
$('#primary-menu a,.scroll').click(function (e) {
    $('html,body').animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 500);
    $('input[name="Name"]').focus();
    e.preventDefault();
});
$(window).on("load", function () {
    if ($(window).width() > 767.98) {
        var height = 0;
        $(".trending_tests_boxes").each(function () {
            if (height < $(this).outerHeight()) {
                height = $(this).outerHeight();
            }
        });
        $(".trending_tests_boxes").css("height", height);
    }
});
$(window).on("load", function () {
    if ($(window).width() > 767.98) {
        var height = 0;
        $(".trending_tests_boxes_image").each(function () {
            if (height < $(this).outerHeight()) {
                height = $(this).outerHeight();
            }
        });
        $(".trending_tests_boxes_image").css("height", height);
    }
});
$(window).on("load", function () {
    if ($(window).width() > 767.98) {
        var height = 0;
        $(".white-background").each(function () {
            if (height < $(this).outerHeight()) {
                height = $(this).outerHeight();
            }
        });
        $(".white-background").css("height", height);
    }
});
$(window).on("load", function () {
    if ($(window).width() > 767.98) {
        var height = 0;
        $(".testimonial-content").each(function () {
            if (height < $(this).outerHeight()) {
                height = $(this).outerHeight();
            }
        });
        $(".testimonial-content").css("height", height);
    }
});
function openNav() {
    document.getElementById("myNav").style.height = "100%";
}
function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}
function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}