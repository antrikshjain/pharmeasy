<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package diabetes
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header id="masthead" class="site-header pt-10 pb-10">
	<div class="site-branding">
		<div class="container">
			<div class="row align-self-center">
				<div class="col-lg-2 col-md-2 col-sm-4 col-4 mt-10 mt-xs-10 text-lg-left text-md-left text-sm-center text-left align-self-center d-xl-block d-lg-block d-md-block d-sm-none d-none">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php the_field ('logo', 'option')?>" alt="" /></a>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-10 text-right d-xl-none d-lg-none d-md-none d-sm-block d-block mobile-container">
					<div class="topnav">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="active text-left"><img width="70%" src="<?php the_field ('logo', 'option')?>" alt="" /></a>
						<div id="myLinks" class="roboto-medium">
							<a class="scroll" href="#all">All</a>
							<a class="scroll" href="#types">Types</a>
							<a class="scroll" href="#diagnosis">Diagnosis</a>
							<a class="scroll" href="#diet">Diet & nutrition</a>
							<a class="scroll" href="#exercise">Exercise</a>
							<a class="scroll" href="#living">Living with Diabetes</a>
							<a class="scroll" href="#complications">Complications</a>
							<a class="scroll" href="#tools">Tools</a>
							<a class="scroll" href="#videos">Videos</a>
						</div>
						<a href="javascript:void(0);" class="icon" onclick="myFunction()">
							<i class="fa fa-bars"></i>
						</a>
					</div>
				</div>
				<nav id="site-navigation" class="align-self-center d-none d-sm-none d-md-block d-lg-block main-navigation col-lg-10 col-md-10 col-sm-10 text-right">
					<div class="menu-menu-1-container">
						<ul id="primary-menu" class="wow fadeInDown fs-15 list-unstyled primary-menu mb-0" data-wow-delay="0.2s">
							<li><a class="scroll" href="#all">All</a></li>
							<li><a class="scroll" href="#types">Types</a></li>
							<li><a class="scroll" href="#diagnosis">Diagnosis</a>
							<li><a class="scroll" href="#diet">Diet & nutrition</a></li>
							<li><a class="scroll" href="#exercise">Exercise</a></li>
							<li><a class="scroll" href="#living">Living with Diabetes</a></li>
							<li><a class="scroll" href="#complications">Complications</a></li>
							<li><a class="scroll" href="#tools">Tools</a></li>
							<li><a class="scroll" href="#videos">Videos</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>
<section class="banner-caption-inner-page pt-50 pb-50">
	<div class="container" id="">
		<div class="row align-self-center">
			<div class="col-lg-7 col-md-7 col-12 align-self-center">
				<h1 class="color-green fs-45 fs-sm-36 fs-xs-30"><?php echo the_field('banner_title');?></h1>
			</div>
			<div class="col-lg-5 col-md-5 col-12 banner-styling align-self-center">
				<img src="<?php echo the_field('banner_image');?>" alt="" />
			</div>
		</div>
	</div>
</section>